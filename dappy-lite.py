##############################################################################
##########  DAPPY CODE CREATED BY ASHTON EAVES
##############################################################################

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#timestep1:
t1 = pd.read_excel('./Matt_files/test2.1.xlsx')
a=t1.values

#timestep2:
t2 = pd.read_excel('./Matt_files/test3.1.xlsx')
b=t2.values

#timestep3:
t3 = pd.read_excel('./Matt_files/test4.1.xlsx')
c=t3.values

#timestep4:
t4 = pd.read_excel('./Matt_files/test5.1.xlsx')
d=t4.values

#timestep5:
t5 = pd.read_excel('./Matt_files/test6.1.xlsx')
e=t5.values

#get weighting:
weight = pd.read_excel('./Matt_files/weighting-1.xlsx')
w8=weight.values



##############################################################################

#Column and row names for individual timestep runs:
    
column_names1 = ['Total damage cost', 'Total Damage Cost Plus Intervention Cost Round 1',
                 'Total Damage Cost Plus Intervention Cost All Rounds', 
                 'Structural Replacement']
row_names1    = ['P1', 'P2', 'P3','P4','P5','P6', 'P7', 'P8', 'P9', 'P10',
                  'P11', 'P12', 'P13', 'P14', 'P15', 'P16', 'P17', 'P18',
                  'P19', 'P20','P21','P22', 'P23', 'P24', 'P25', 'P26', 'P27', 'P28',
                  'P29', 'P30', 'P31', 'P32', 'sum', 'rank']

column_names2 = ['Total damage cost', 'Total Damage Cost Plus Intervention Cost Round 1',
                 'Total Damage Cost Plus Intervention Cost All Rounds', 
                 'Structural Replacement', 'sum', 'rank']
row_names2    = ['P1', 'P2', 'P3', 'P4', 'P5', 'P6', 'P7', 'P8', 'P9', 'P10',
                  'P11', 'P12', 'P13', 'P14', 'P15', 'P16', 'P17', 'P18',
                  'P19', 'P20', 'P21', 'P22', 'P23', 'P24', 'P25', 'P26', 'P27', 'P28',
                  'P29', 'P30', 'P31', 'P32']

##############################################################################

for i in [a]:
             
#multiply values with weighting:
    wa = np.zeros((32,4))
    wa = np.multiply(i,w8)
#sum columns and rows:
    suma0 = np.sum(wa,axis=0)
    suma1 = np.sum(wa,axis=1)
 #find the minimum values for rows and columns:
    col_min=np.min(wa)
    col_min0=a.min(0)
    row_min=np.min(wa[:,3])
    row_min0=a.min(1)
#difference or least regret between scenarios and policies:
    wa = np.array(wa)
    c = np.min(wa,axis=0)
    r = np.min(wa,axis=1)
    cidx = np.argmin(wa,axis=0)
    ridx = np.argmin(wa,axis=1)
    cdif = wa-c
    rdif = wa-r[:,None]
#find the sum of the rows and columns for the difference arrays:
    sumc = np.sum(cdif,axis=0)
    sumr = np.sum(rdif,axis=1)
    sumr1 = np.reshape(sumr,(32,1))
#append the scenario array with the column sums:
    sumcol = np.zeros((33,4))
    sumcol = np.append([cdif],[sumc])
    sumcol.shape = (33,4)
#rank columns:
    order0 = sumc.argsort()
    rank_a = order0.argsort()
    rankcol_a = np.zeros((34,4))
    rankcol_a = np.append([sumcol],[rank_a])
    rankcol_a.shape = (34,4)
#append the policy array with row sums
    sumrow = np.zeros((32,5))
    sumrow = np.hstack((rdif,sumr1))
#rank rows
    order1 = sumr.argsort()
    rank_a1 = order1.argsort()
    rank_a1r = np.reshape(rank_a1,(32,1))
    rankrow_a = np.zeros((32,6))
    rankrow_a = np.hstack((sumrow,rank_a1r))
#Add row and column headers for least regret for df0:
    df0 = np.zeros((34,5))
    df0 = pd.DataFrame(rankcol_a, columns=column_names1, index=row_names1)
#Add row and column headers for least regret for df1:
    df1 = np.zeros((33,7))
    df1 = pd.DataFrame(rankrow_a, columns=column_names2, index=row_names2)

##############################################################################

for j in [b]:
    
#multiply values with weighting:
    wb = np.zeros((32,4))
    wb = np.multiply(j,w8)
#sum columns and rows:
    suma0 = np.sum(wb,axis=0)
    suma1 = np.sum(wb,axis=1)    
#find the minimum values for rows and columns:
    col_min=np.min(wb)
    col_min0=a.min(0)
    row_min=np.min(wb[:,3])
    row_min0=a.min(1)
#difference or least regret between scenarios and policies:
    wb = np.array(wb)
    c = np.min(wb,axis=0)
    r = np.min(wb,axis=1)
    cidx = np.argmin(wb,axis=0)
    ridx = np.argmin(wb,axis=1)
    cdif = wb-c
    rdif = wb-r[:,None]
#find the sum of the rows and columns for the difference arrays:
    sumc = np.sum(cdif,axis=0)
    sumr = np.sum(rdif,axis=1)
    sumr1 = np.reshape(sumr,(32,1))
#append the scenario array with the column sums:
    sumcol = np.zeros((33,4))
    sumcol = np.append([cdif],[sumc])
    sumcol.shape = (33,4)
#rank columns:
    order0 = sumc.argsort()
    rank_b = order0.argsort()
    rankcol_b = np.zeros((34,4))
    rankcol_b = np.append([sumcol],[rank_b])
    rankcol_b.shape = (34,4)
#append the policy array with row sums
    sumrow = np.zeros((32,5))
    sumrow = np.hstack((rdif,sumr1))
#rank rows
    order1 = sumr.argsort()
    rank_b1 = order1.argsort()
    rank_b1r = np.reshape(rank_b1,(32,1))
    rankrow_b = np.zeros((32,6))
    rankrow_b = np.hstack((sumrow,rank_b1r))
#Add row and column headers for least regret for df0:
    df2 = np.zeros((34,5))
    df2 = pd.DataFrame(rankcol_b, columns=column_names1, index=row_names1)
#Add row and column headers for least regret for df1:
    df3 = np.zeros((33,7))
    df3 = pd.DataFrame(rankrow_b, columns=column_names2, index=row_names2)

##############################################################################

for k in [c]:
    
#multiply values with weighting:
    wa = np.zeros((32,4))
    wa = np.multiply(i,w8)
#sum columns and rows:
    suma0 = np.sum(wa,axis=0)
    suma1 = np.sum(wa,axis=1)
 #find the minimum values for rows and columns:
    col_min=np.min(wa)
    col_min0=a.min(0)
    row_min=np.min(wa[:,3])
    row_min0=a.min(1)
#difference or least regret between scenarios and policies:
    wa = np.array(wa)
    c = np.min(wa,axis=0)
    r = np.min(wa,axis=1)
    cidx = np.argmin(wa,axis=0)
    ridx = np.argmin(wa,axis=1)
    cdif = wa-c
    rdif = wa-r[:,None]
#find the sum of the rows and columns for the difference arrays:
    sumc = np.sum(cdif,axis=0)
    sumr = np.sum(rdif,axis=1)
    sumr1 = np.reshape(sumr,(32,1))
#append the scenario array with the column sums:
    sumcol = np.zeros((33,4))
    sumcol = np.append([cdif],[sumc])
    sumcol.shape = (33,4)
#rank columns:
    order0 = sumc.argsort()
    rank_c = order0.argsort()
    rankcol_c = np.zeros((34,4))
    rankcol_c = np.append([sumcol],[rank_c])
    rankcol_c.shape = (34,4)
#append the policy array with row sums
    sumrow = np.zeros((32,5))
    sumrow = np.hstack((rdif,sumr1))
#rank rows
    order1 = sumr.argsort()
    rank_c1 = order1.argsort()
    rank_c1r = np.reshape(rank_c1,(32,1))
    rankrow_c = np.zeros((32,6))
    rankrow_c = np.hstack((sumrow,rank_c1r))
#Add row and column headers for least regret for df0:
    df4 = np.zeros((34,5))
    df4 = pd.DataFrame(rankcol_c, columns=column_names1, index=row_names1)
#Add row and column headers for least regret for df1:
    df5 = np.zeros((33,7))
    df5 = pd.DataFrame(rankrow_c, columns=column_names2, index=row_names2)
    
##############################################################################
##############################################################################

for l in [d]:
    
#multiply values with weighting:
    wc = np.zeros((32,4))
    wc = np.multiply(l,w8)
#sum columns and rows:
    suma0 = np.sum(wc,axis=0)
    suma1 = np.sum(wc,axis=1)    
#find the minimum values for rows and columns:
    col_min=np.min(wc)
    col_min0=a.min(0)
    row_min=np.min(wc[:,3])
    row_min0=a.min(1)
#difference or least regret between scenarios and policies:
    wc = np.array(wc)
    c = np.min(wc,axis=0)
    r = np.min(wc,axis=1)
    cidx = np.argmin(wc,axis=0)
    ridx = np.argmin(wc,axis=1)
    cdif = wc-c
    rdif = wc-r[:,None]
#find the sum of the rows and columns for the difference arrays:
    sumc = np.sum(cdif,axis=0)
    sumr = np.sum(rdif,axis=1)
    sumr1 = np.reshape(sumr,(32,1))
#append the scenario array with the column sums:
    sumcol = np.zeros((33,4))
    sumcol = np.append([cdif],[sumc])
    sumcol.shape = (33,4)
#rank columns:
    order0 = sumc.argsort()
    rank_d = order0.argsort()
    rankcol_d = np.zeros((34,4))
    rankcol_d = np.append([sumcol],[rank_d])
    rankcol_d.shape = (34,4)
#append the policy array with row sums
    sumrow = np.zeros((32,5))
    sumrow = np.hstack((rdif,sumr1))
#rank rows
    order1 = sumr.argsort()
    rank_d1 = order1.argsort()
    rank_d1r = np.reshape(rank_d1,(32,1))
    rankrow_d = np.zeros((32,6))
    rankrow_d = np.hstack((sumrow,rank_d1r))    
#Add row and column headers for least regret for df0:
    df6 = np.zeros((34,5))
    df6 = pd.DataFrame(rankcol_d, columns=column_names1, index=row_names1)
#Add row and column headers for least regret for df1:
    df7 = np.zeros((33,7))
    df7 = pd.DataFrame(rankrow_d, columns=column_names2, index=row_names2)
    
##############################################################################
##############################################################################

for m in [e]:
    
#multiply values with weighting:
    wc = np.zeros((32,4))
    wc = np.multiply(m,w8)
#sum columns and rows:
    suma0 = np.sum(wc,axis=0)
    suma1 = np.sum(wc,axis=1)    
#find the minimum values for rows and columns:
    col_min=np.min(wc)
    col_min0=a.min(0)
    row_min=np.min(wc[:,3])
    row_min0=a.min(1)
#difference or least regret between scenarios and policies:
    wc = np.array(wc)
    c = np.min(wc,axis=0)
    r = np.min(wc,axis=1)
    cidx = np.argmin(wc,axis=0)
    ridx = np.argmin(wc,axis=1)
    cdif = wc-c
    rdif = wc-r[:,None]
#find the sum of the rows and columns for the difference arrays:
    sumc = np.sum(cdif,axis=0)
    sumr = np.sum(rdif,axis=1)
    sumr1 = np.reshape(sumr,(32,1))
#append the scenario array with the column sums:
    sumcol = np.zeros((33,4))
    sumcol = np.append([cdif],[sumc])
    sumcol.shape = (33,4)
#rank columns:
    order0 = sumc.argsort()
    rank_e = order0.argsort()
    rankcol_e = np.zeros((34,4))
    rankcol_e = np.append([sumcol],[rank_e])
    rankcol_e.shape = (34,4)
#append the policy array with row sums
    sumrow = np.zeros((32,5))
    sumrow = np.hstack((rdif,sumr1))
#rank rows
    order1 = sumr.argsort()
    rank_e1 = order1.argsort()
    rank_e1r = np.reshape(rank_e1,(32,1))
    rankrow_e = np.zeros((32,6))
    rankrow_e = np.hstack((sumrow,rank_e1r))    
#Add row and column headers for least regret for df0:
    df8 = np.zeros((34,5))
    df8 = pd.DataFrame(rankcol_e, columns=column_names1, index=row_names1)
#Add row and column headers for least regret for df1:
    df9 = np.zeros((33,7))
    df9 = pd.DataFrame(rankrow_e, columns=column_names2, index=row_names2)
    
##############################################################################

y = a+b+c+d+e
for x in [y]:
    
#multiply values with weighting:
    wc = np.zeros((32,4))
    wc = np.multiply(x,w8)
#sum columns and rows:
    suma0 = np.sum(wc,axis=0)
    suma1 = np.sum(wc,axis=1)    
#find the minimum values for rows and columns:
    col_min=np.min(wc)
    col_min0=a.min(0)
    row_min=np.min(wc[:,3])
    row_min0=a.min(1)
#difference or least regret between scenarios and policies:
    wc = np.array(wc)
    c = np.min(wc,axis=0)
    r = np.min(wc,axis=1)
    cidx = np.argmin(wc,axis=0)
    ridx = np.argmin(wc,axis=1)
    cdif = wc-c
    rdif = wc-r[:,None]
#find the sum of the rows and columns for the difference arrays:
    sumc = np.sum(cdif,axis=0)
    sumr = np.sum(rdif,axis=1)
    sumr1 = np.reshape(sumr,(32,1))
#append the scenario array with the column sums:
    sumcol = np.zeros((33,4))
    sumcol = np.append([cdif],[sumc])
    sumcol.shape = (33,4)
#rank columns:
    order0 = sumc.argsort()
    rank_x = order0.argsort()
    rankcol_x = np.zeros((34,4))
    rankcol_x = np.append([sumcol],[rank_x])
    rankcol_x.shape = (34,4)
#append the policy array with row sums
    sumrow = np.zeros((32,5))
    sumrow = np.hstack((rdif,sumr1))
#rank rows
    order1 = sumr.argsort()
    rank_x1 = order1.argsort()
    rank_x1r = np.reshape(rank_e1,(32,1))
    rankrow_x = np.zeros((32,6))
    rankrow_x = np.hstack((sumrow,rank_x1r))    
#Add row and column headers for least regret:
    df10 = np.zeros((34,5))
    df10 = pd.DataFrame(rankcol_x, columns=column_names1, index=row_names1)
    
#Add row and column headers for least regret:
    df11 = np.zeros((33,7))
    df11 = pd.DataFrame(rankrow_x, columns=column_names2, index=row_names2)
      
##############################################################################
#############   RESULTS
##############################################################################

#Print best scenario and policy for each timestep

#Min scenario value by index for each timestep:
    
    print(df0[-1:])
    df0_scenario_rank = df0.idxmin()
    print('Scenario rank T1', df0_scenario_rank)
    print(df2[-1:])
    df2_scenario_rank = df2.idxmin()
    print('Scenario rank T2', df2_scenario_rank)
    print(df4[-1:])
    df4_scenario_rank = df4.idxmin()
    print('Scenario rank T3', df4_scenario_rank)
    print(df6[-1:])
    df6_scenario_rank = df6.idxmin()
    print('Scenario rank T4', df6_scenario_rank) 
    print(df8[-1:])
    df8_scenario_rank = df8.idxmin()
    print('Scenario rank T5', df8_scenario_rank)
    
#Descriptive statistics:
    print('Policy of least regret across all scenarios T1:'
        , df0_scenario_rank.describe())
    print('Policy of least regret across all scenarios T2:'
          , df2_scenario_rank.describe())
    print('Policy of least regret across all scenarios T3:'
          , df4_scenario_rank.describe())   
    print('Policy of least regret across all scenarios T4:'
          , df6_scenario_rank.describe())   
    print('Policy of least regret across all scenarios T5:'
          , df8_scenario_rank.describe())   

#Min policy value by index for each timestep: 
    df1_policy_rank = df1.idxmin(axis=1)
    print('policy rank T1', df1_policy_rank)
    df3_policy_rank = df3.idxmin(axis=1)
    print('policy rank T2', df3_policy_rank)
    df5_policy_rank = df5.idxmin(axis=1)
    print('policy rank T3', df5_policy_rank)
    df7_policy_rank = df7.idxmin(axis=1)
    print('policy rank T4', df7_policy_rank)
    df9_policy_rank = df9.idxmin(axis=1)
    print('policy rank T5', df9_policy_rank)
    
#Min policy value by index for each timestep: 
    print('df10 = all timesteps', df10)
    print('df11 = all timesteps', df11)
    
#Descriptive statistics:
    print('Scenario of least regret across all policies T1:'
        , df1_policy_rank.describe())
    print('Scenario of least regret across all policies T2:'
          , df3_policy_rank.describe())   
    print('Scenario of least regret across all policies T3:'
          , df5_policy_rank.describe())
    print('Scenario of least regret across all policies T4:'
          , df7_policy_rank.describe())
    print('Scenario of least regret across all policies T5:'
          , df9_policy_rank.describe())

##############################################################################
#Print best scenario over all timesteps:

#Min scenario value by index
    print(df10[-1:])
    df10_scenario_rank = df10.idxmin()
    print('Scenario rank', df10_scenario_rank)
    
#Descriptive statistics
    print('Policy of least regret across all scenarios:'
          , df10_scenario_rank.describe())
           
#Boxplot for policies
    plt.figure(figsize=(12, 10), dpi= 80, facecolor='g', edgecolor='k')
    plt.boxplot(df10[:-2])
    plt.title('Boxplot of scenarios across policies')
    plt.xlabel('Policy')
    plt.ylabel('Value')
    plt.show()
    
##############################################################################
#Print best policy over all timesteps:

#Min scenario value by index
    df11_policy_rank = df11.idxmin(axis=1)
    print('policy rank', df11_policy_rank)
    
#Descriptive statistics
    print('Scenario of least regret across all policies:'
          , df11_policy_rank.describe())
     
#Boxplot for policies   
    df11t = df11.transpose()
    plt.figure(figsize=(12, 10), dpi= 80, facecolor='y', edgecolor='k')
    plt.boxplot(df11t[:-2])
    plt.title('Boxplot of policies across scenarios')
    plt.xlabel('Scenario')
    plt.ylabel('Value')
    plt.show()
    
#Save as csv with pandas:

    df10.to_csv('./Matt_files/out-scenario.csv', sep=',')
    df11.to_csv('./Matt_files/out-policy.csv', sep=',')


##############################################################################
##############################################################################
